package leo.caculator;

import java.util.Stack;

/**
 * Created by leonardzhou on 15/7/8.
 */
public class Caculator {

    private Stack<Float> numberStack;

    private Stack<Operator> operatorStack;


    public Caculator() {
        numberStack = new Stack<Float>();
        operatorStack = new Stack<Operator>();
    }

    public float caculate(String expression) {
        scan(expression);
        while (!operatorStack.empty()) {
            Operator op = operatorStack.pop();
            int opCnt = op.getOpCnt();
            float[] args = new float[opCnt];
            for (int i = 0; i < opCnt; i++) {
                args[opCnt - i - 1] = numberStack.pop();
            }
            float result = op.compute(args);
            numberStack.push(result);
        }
        return numberStack.peek().floatValue();
    }

    private void scan(String expression) {
        StringBuilder word = new StringBuilder(10);
        for (int i = 0; i < expression.length(); i++) {
            final char chr = expression.charAt(i);
            Operator operator = Operator.toOperator(chr);
            if (null != operator) {
                boolean isNewAdded = newNumber(word);
                newOperator(operator);
            } else if (' ' == chr) {
                newNumber(word);
                continue;
            } else if (chr >= '0' && chr <= '9' || chr == '.') {
                word.append(chr);
            }
        }
        newNumber(word);
    }


    private boolean newNumber(StringBuilder word) {
        boolean newAdded = false;
        if (word.length() > 0) {
            numberStack.push(Float.valueOf(word.toString()));
            word.delete(0, word.length());
            newAdded = true;
        }
        return newAdded;
    }

    private void newOperator(Operator operator) {
        if (operatorStack.empty()) {
            operatorStack.push(operator);
        } else if (operator.equals(Operator.BraLeft)) {
            operatorStack.push(operator);

        } else if (operator.equals(Operator.BraRight)) {
            Operator op = operatorStack.pop();
            while (!op.equals(Operator.BraLeft)) {

                int opCnt = op.getOpCnt();
                float[] args = new float[opCnt];
                for (int i = 0; i < opCnt; i++) {
                    args[opCnt - i - 1] = numberStack.pop();
                }
                float result = op.compute(args);
                numberStack.push(result);
                op = operatorStack.pop();
            }


        } else {
            if (operatorStack.peek().getPri() < operator.getPri()) {
                operatorStack.push(operator);
            } else {
                Operator op = operatorStack.pop();
                int opCnt = op.getOpCnt();
                float[] args = new float[opCnt];
                for (int i = 0; i < opCnt; i++) {
                    args[opCnt - i - 1] = numberStack.pop();
                }
                float result = op.compute(args);
                numberStack.push(result);
                newOperator(operator);
            }
        }
    }
}
