package leo.caculator;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Main {

    public static void main(String[] args) throws Exception {
        String expression = null;
        while (!"q".equals(expression)) {
            System.out.println("please enter expression,q to quit:");
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            expression = reader.readLine();
            float result = new Caculator().caculate(expression);
            System.out.println("caculated result:" + result);
        }

    }
}
