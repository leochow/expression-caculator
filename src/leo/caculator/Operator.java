package leo.caculator;

/**
 * Created by leonardzhou on 15/7/8.
 */
public enum Operator {
    Add {
        public int getPri() {
            return 1;
        }

        public int getOpCnt() {
            return 2;
        }

        public float compute(float... ops) {
            return ops[0] + ops[1];
        }
    },
    Minus {
        public int getPri() {
            return 1;
        }

        public int getOpCnt() {
            return 2;
        }

        public float compute(float... ops) {
            return ops[0] - ops[1];
        }
    }, Multiply {
        public int getPri() {
            return 2;
        }

        public int getOpCnt() {
            return 2;
        }

        public float compute(float... ops) {
            return ops[0] * ops[1];
        }
    }, Divide {
        public int getPri() {
            return 2;
        }

        public int getOpCnt() {
            return 2;
        }

        public float compute(float... ops) {
            return ops[0] / ops[1];
        }
    }, BraLeft {
        public int getPri() {
            return 0;
        }

        public int getOpCnt() {
            return 0;
        }

        public float compute(float... ops) {
            return 1;
        }
    }, BraRight {
        public int getPri() {
            return 0;
        }

        public int getOpCnt() {
            return 0;
        }

        public float compute(float... ops) {
            return 1;
        }
    }, Negative {
        public int getPri() {
            return 3;
        }

        public int getOpCnt() {
            return 1;
        }

        public float compute(float... ops) {
            return -ops[0];
        }
    }, Mod {
        public int getPri() {
            return 2;
        }

        public int getOpCnt() {
            return 2;
        }

        public float compute(float... ops) {
            return ops[0] % ops[1];
        }
    },X{
        public int getPri() {
            return 2;
        }

        public int getOpCnt() {
            return 3;
        }

        public float compute(float... ops) {
            return ops[0] * ops[1] * ops[2];
        }
    };

    public abstract int getPri();

    public abstract int getOpCnt();

    public abstract float compute(float... ops);


    public static Operator toOperator(char op) {
        switch (op) {
            case '+':
                return Add;
            case '-':
                return Minus;
            case '*':
                return Multiply;
            case '/':
                return Divide;
            case '(':
                return BraLeft;
            case ')':
                return BraRight;
            case '!':
                return Negative;
            case '%':
                return Mod;
            case 'x':
                return X;
        }
        return null;
    }
}
